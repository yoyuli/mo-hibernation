
import subprocess

with open('list.txt') as file:
    repos = file.read().splitlines()

f = open('execute.sh', 'w')
print('#!/bin/sh',file=f)

for repo in repos:
    url='git@bitbucket.org:cloudreach/%s.git' % repo
    ## Clone the repository
    print("git", "clone",url,file=f)

    print("cd",repo,file=f)

    # Track all branches
    print("for i in `git branch -a | grep remote | grep -v HEAD | grep -v master`; do git branch --track ${i#remotes/origin/} $i; done",file=f)
    
    # Fetch and pull all branches
    print("git fetch --all;git pull --all",file=f)

    # Create bundle
    print("git","bundle","create","%s.bundle" % repo,"--all",file=f)

    # Move bundle the parent directory
    print("mv", "%s.bundle" % repo,"../bundles",file=f)

    print("cd ..",file=f)
    
    # Delete cloned repo
    print("rm -rf %s" % repo,file=f)
    