#!/bin/sh
git clone git@bitbucket.org:cloudreach/mobg-pp-gpp-regridding-wafc.git
cd mobg-pp-gpp-regridding-wafc
for i in `git branch -a | grep remote | grep -v HEAD | grep -v master`; do git branch --track ${i#remotes/origin/} $i; done
git fetch --all;git pull --all
git bundle create mobg-pp-gpp-regridding-wafc.bundle --all
mv mobg-pp-gpp-regridding-wafc.bundle ../bundles
cd ..
rm -rf mobg-pp-gpp-regridding-wafc
