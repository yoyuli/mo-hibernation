

ssh_jenkins:
	ssh -i /Users/yoyuli/GoogleDrive/Hibernation/credentials/mobg-ncl-buildkey.pem ec2-user@10.127.200.184

ssh_hbase:
	ssh -i /Users/yoyuli/GoogleDrive/Hibernation/credentials/mobg-ncl-buildkey.pem hadoop@ip-10-127-229-214.eu-west-2.compute.internal

copy_kubeconfig:
	ssh -i /Users/yoyuli/GoogleDrive/Hibernation/credentials/mobg-ncl-buildkey.pem \
	ec2-user@10.127.200.184 "sudo cat /var/lib/jenkins/workspace/mobg-ncl-dev-k8s/kubeconfig" \
	> ~/.kube/k8s_test
	export KUBECONFIG=~/.kube/config:~/.kube/k8s_test
	kubectl config use k8s.test.bg.api.metoffice.cloud


put_object:
	aws s3api put-object \
	--bucket mobg-dsa-test-data-mo-atmospheric-mogreps-g \
	--body /Users/yoyuli/Downloads/20180111T1500Z-PT0003H-soil_mass_concentration_of_water_on_soil_levels.nc \
	--key test_file.nc \
	--tagging "forecast_reference_time=2019-02-12T20:00:00Z00:00&forecast_period=3600&name=moisture_content_of_soil_layer"

put_object_2:
	aws s3api put-object \
	--bucket mobg-dsa-test-data-mo-atmospheric-global \
	--body /Users/yoyuli/Downloads/774c9b1c-a4b1-40b0-b68b-e053d21ae3b4 \
	--key test_file_2.nc \
	--tagging "forecast_reference_time=2019-02-12T20:00:00+00:00&forecast_period=360000&name=deprecated_precipitation_accumulation"


delete_cluster:
	kops delete cluster --name k8s.test.bg.api.metoffice.cloud --state s3://mobg-ncl-test-k8s-kops --yes
	sceptre --var-file=environments/test.yaml --var "cluster_name=k8s.test.bg.api.metoffice.cloud" delete-env k8s





sceptre --var-file=environments/test.yaml --var "ubuntu_ami=" --var "DB_PASSWORD="  --var "cluster_name=k8s.test.bg.api.metoffice.cloud"  delete-env ''
sceptre --var-file=environments/dev.yaml --var "ubuntu_ami=" --var "DB_PASSWORD="  --var "cluster_name=k8s.dev.bg.api.metoffice.cloud"  delete-env k8s

sceptre --var-file=environments/dev.yaml --var "ubuntu_ami=" --var "DB_PASSWORD="  --var "cluster_name=k8s.dev.bg.api.metoffice.cloud"  delete-env preparation
